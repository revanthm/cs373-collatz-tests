#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# Project 1: Collatz
# Names: Jordan Ferguson (jnf723) and Davie Nguyen


# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # Additional Tests

    def test_read_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 200)

    def test_read_3(self):
        s = "201 210\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 201)
        self.assertEqual(j, 210)

    def test_read_4(self):
        s = "900 1000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  900)
        self.assertEqual(j, 1000)

    def test_read_5(self):
        s = "654456 965548\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 654456)
        self.assertEqual(j, 965548)

    def test_read_6(self):
        s = "75444 999\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 75444)
        self.assertEqual(j,   999)

    def test_read_7(self):
        s = "3278 59433\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  3278)
        self.assertEqual(j, 59433)

    def test_read_8(self):
        s = "2001 3000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 2001)
        self.assertEqual(j, 3000)

    def test_read_9(self):
        s = "3001 5555\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 3001)
        self.assertEqual(j, 5555)

    def test_read_10(self):
        s = "300 6000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  300)
        self.assertEqual(j, 6000)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # Additional Tests

    def test_eval_5(self):
        v = collatz_eval(654456, 965548)
        self.assertEqual(v, 525)

    def test_eval_6(self):
        v = collatz_eval(75444, 999)
        self.assertEqual(v, 340)

    def test_eval_7(self):
        v = collatz_eval(3278, 59433)
        self.assertEqual(v, 340)

    def test_eval_8(self):
        v = collatz_eval(2001, 3000)
        self.assertEqual(v, 217)

    def test_eval_9(self):
        v = collatz_eval(3001, 5555)
        self.assertEqual(v, 238)

    def test_eval_10(self):
        v = collatz_eval(300, 6000)
        self.assertEqual(v, 238)


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # Additional Tests

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")

    def test_print_5(self):
        w = StringIO()
        collatz_print(w, 654456, 965548, 525)
        self.assertEqual(w.getvalue(), "654456 965548 525\n")

    def test_print_6(self):
        w = StringIO()
        collatz_print(w, 75444, 999, 340)
        self.assertEqual(w.getvalue(), "75444 999 340\n")

    def test_print_7(self):
        w = StringIO()
        collatz_print(w, 3278, 59433, 340)
        self.assertEqual(w.getvalue(), "3278 59433 340\n")

    def test_print_8(self):
        w = StringIO()
        collatz_print(w, 2001, 3000, 217)
        self.assertEqual(w.getvalue(), "2001 3000 217\n")

    def test_print_9(self):
        w = StringIO()
        collatz_print(w, 3001, 5555, 238)
        self.assertEqual(w.getvalue(), "3001 5555 238\n")

    def test_print_10(self):
        w = StringIO()
        collatz_print(w, 300, 6000, 238)
        self.assertEqual(w.getvalue(), "300 6000 238\n")


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    # Additional Tests

    def test_solve_2(self):
        r = StringIO("654456 965548\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "654456 965548 525\n")

    def test_solve_3(self):
        r = StringIO("75444 999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "75444 999 340\n")

    def test_solve_4(self):
        r = StringIO("3278 59433\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "3278 59433 340\n")

    def test_solve_5(self):
        r = StringIO("2001 3000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2001 3000 217\n")

    def test_solve_6(self):
        r = StringIO("3001 5555\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "3001 5555 238\n")

    def test_solve_7(self):
        r = StringIO("300 6000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "300 6000 238\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
